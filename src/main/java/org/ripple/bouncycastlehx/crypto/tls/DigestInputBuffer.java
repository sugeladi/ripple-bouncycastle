package org.ripple.bouncycastlehx.crypto.tls;

import java.io.ByteArrayOutputStream;

import org.ripple.bouncycastlehx.crypto.Digest;

class DigestInputBuffer extends ByteArrayOutputStream
{
    void updateDigest(Digest d)
    {
        d.update(this.buf, 0, count);
    }
}

package org.ripple.bouncycastlehx.crypto.tls;

import java.io.IOException;

import org.ripple.bouncycastlehx.crypto.params.AsymmetricKeyParameter;

public interface TlsAgreementCredentials
    extends TlsCredentials
{
    byte[] generateAgreement(AsymmetricKeyParameter peerPublicKey)
        throws IOException;
}

package org.ripple.bouncycastlehx.crypto.tls;

import org.ripple.bouncycastlehx.crypto.DSA;
import org.ripple.bouncycastlehx.crypto.params.AsymmetricKeyParameter;
import org.ripple.bouncycastlehx.crypto.params.DSAPublicKeyParameters;
import org.ripple.bouncycastlehx.crypto.signers.DSASigner;
import org.ripple.bouncycastlehx.crypto.signers.HMacDSAKCalculator;

public class TlsDSSSigner
    extends TlsDSASigner
{
    public boolean isValidPublicKey(AsymmetricKeyParameter publicKey)
    {
        return publicKey instanceof DSAPublicKeyParameters;
    }

    protected DSA createDSAImpl(short hashAlgorithm)
    {
        return new DSASigner(new HMacDSAKCalculator(TlsUtils.createHash(hashAlgorithm)));
    }

    protected short getSignatureAlgorithm()
    {
        return SignatureAlgorithm.dsa;
    }
}

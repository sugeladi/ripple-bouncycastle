package org.ripple.bouncycastlehx.crypto.tls;

import org.ripple.bouncycastlehx.crypto.DSA;
import org.ripple.bouncycastlehx.crypto.params.AsymmetricKeyParameter;
import org.ripple.bouncycastlehx.crypto.params.ECPublicKeyParameters;
import org.ripple.bouncycastlehx.crypto.signers.ECDSASigner;
import org.ripple.bouncycastlehx.crypto.signers.HMacDSAKCalculator;

public class TlsECDSASigner
    extends TlsDSASigner
{
    public boolean isValidPublicKey(AsymmetricKeyParameter publicKey)
    {
        return publicKey instanceof ECPublicKeyParameters;
    }

    protected DSA createDSAImpl(short hashAlgorithm)
    {
        return new ECDSASigner(new HMacDSAKCalculator(TlsUtils.createHash(hashAlgorithm)));
    }

    protected short getSignatureAlgorithm()
    {
        return SignatureAlgorithm.ecdsa;
    }
}

package org.ripple.bouncycastlehx.crypto.tls;

public interface TlsCredentials
{
    Certificate getCertificate();
}

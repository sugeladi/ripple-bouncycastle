package org.ripple.bouncycastlehx.crypto.ec;

import org.ripple.bouncycastlehx.crypto.CipherParameters;

public interface ECPairTransform
{
    void init(CipherParameters params);

    ECPair transform(ECPair cipherText);
}

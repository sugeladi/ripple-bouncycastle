package org.ripple.bouncycastlehx.crypto.ec;

import org.ripple.bouncycastlehx.crypto.CipherParameters;
import org.ripple.bouncycastlehx.math.ec.ECPoint;

public interface ECEncryptor
{
    void init(CipherParameters params);

    ECPair encrypt(ECPoint point);
}

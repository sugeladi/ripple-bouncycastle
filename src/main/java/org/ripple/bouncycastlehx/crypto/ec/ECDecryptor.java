package org.ripple.bouncycastlehx.crypto.ec;

import org.ripple.bouncycastlehx.crypto.CipherParameters;
import org.ripple.bouncycastlehx.math.ec.ECPoint;

public interface ECDecryptor
{
    void init(CipherParameters params);

    ECPoint decrypt(ECPair cipherText);
}

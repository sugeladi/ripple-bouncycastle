package org.ripple.bouncycastlehx.crypto;

import java.io.IOException;
import java.io.InputStream;

import org.ripple.bouncycastlehx.crypto.params.AsymmetricKeyParameter;

public interface KeyParser
{
    AsymmetricKeyParameter readKey(InputStream stream)
        throws IOException;
}

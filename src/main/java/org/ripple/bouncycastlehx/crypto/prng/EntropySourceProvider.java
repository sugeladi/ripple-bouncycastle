package org.ripple.bouncycastlehx.crypto.prng;

public interface EntropySourceProvider
{
    EntropySource get(final int bitsRequired);
}

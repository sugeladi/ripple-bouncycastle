package org.ripple.bouncycastlehx.crypto.prng;

import org.ripple.bouncycastlehx.crypto.prng.drbg.SP80090DRBG;

interface DRBGProvider
{
    SP80090DRBG get(EntropySource entropySource);
}

package org.ripple.bouncycastlehx.crypto.generators;

import java.math.BigInteger;

import org.ripple.bouncycastlehx.crypto.AsymmetricCipherKeyPair;
import org.ripple.bouncycastlehx.crypto.AsymmetricCipherKeyPairGenerator;
import org.ripple.bouncycastlehx.crypto.KeyGenerationParameters;
import org.ripple.bouncycastlehx.crypto.params.DHParameters;
import org.ripple.bouncycastlehx.crypto.params.ElGamalKeyGenerationParameters;
import org.ripple.bouncycastlehx.crypto.params.ElGamalParameters;
import org.ripple.bouncycastlehx.crypto.params.ElGamalPrivateKeyParameters;
import org.ripple.bouncycastlehx.crypto.params.ElGamalPublicKeyParameters;

/**
 * a ElGamal key pair generator.
 * <p>
 * This generates keys consistent for use with ElGamal as described in
 * page 164 of "Handbook of Applied Cryptography".
 */
public class ElGamalKeyPairGenerator
    implements AsymmetricCipherKeyPairGenerator
{
    private ElGamalKeyGenerationParameters param;

    public void init(
        KeyGenerationParameters param)
    {
        this.param = (ElGamalKeyGenerationParameters)param;
    }

    public AsymmetricCipherKeyPair generateKeyPair()
    {
        DHKeyGeneratorHelper helper = DHKeyGeneratorHelper.INSTANCE;
        ElGamalParameters egp = param.getParameters();
        DHParameters dhp = new DHParameters(egp.getP(), egp.getG(), null, egp.getL());  

        BigInteger x = helper.calculatePrivate(dhp, param.getRandom()); 
        BigInteger y = helper.calculatePublic(dhp, x);

        return new AsymmetricCipherKeyPair(
            new ElGamalPublicKeyParameters(y, egp),
            new ElGamalPrivateKeyParameters(x, egp));
    }
}

package org.ripple.bouncycastlehx.crypto.generators;

import java.math.BigInteger;
import java.security.SecureRandom;

import org.ripple.bouncycastlehx.crypto.AsymmetricCipherKeyPair;
import org.ripple.bouncycastlehx.crypto.AsymmetricCipherKeyPairGenerator;
import org.ripple.bouncycastlehx.crypto.KeyGenerationParameters;
import org.ripple.bouncycastlehx.crypto.params.ECDomainParameters;
import org.ripple.bouncycastlehx.crypto.params.ECKeyGenerationParameters;
import org.ripple.bouncycastlehx.crypto.params.ECPrivateKeyParameters;
import org.ripple.bouncycastlehx.crypto.params.ECPublicKeyParameters;
import org.ripple.bouncycastlehx.math.ec.ECConstants;
import org.ripple.bouncycastlehx.math.ec.ECMultiplier;
import org.ripple.bouncycastlehx.math.ec.ECPoint;
import org.ripple.bouncycastlehx.math.ec.FixedPointCombMultiplier;
import org.ripple.bouncycastlehx.math.ec.WNafUtil;

public class ECKeyPairGenerator
    implements AsymmetricCipherKeyPairGenerator, ECConstants
{
    ECDomainParameters  params;
    SecureRandom        random;

    public void init(
        KeyGenerationParameters param)
    {
        ECKeyGenerationParameters  ecP = (ECKeyGenerationParameters)param;

        this.random = ecP.getRandom();
        this.params = ecP.getDomainParameters();

        if (this.random == null)
        {
            this.random = new SecureRandom();
        }
    }

    /**
     * Given the domain parameters this routine generates an EC key
     * pair in accordance with X9.62 section 5.2.1 pages 26, 27.
     */
    public AsymmetricCipherKeyPair generateKeyPair()
    {
        BigInteger n = params.getN();
        int nBitLength = n.bitLength();
        int minWeight = nBitLength >>> 2;

        BigInteger d;
        for (;;)
        {
            d = new BigInteger(nBitLength, random);

            if (d.compareTo(TWO) < 0  || (d.compareTo(n) >= 0))
            {
                continue;
            }

            /*
             * Require a minimum weight of the NAF representation, since low-weight primes may be
             * weak against a version of the number-field-sieve for the discrete-logarithm-problem.
             * 
             * See "The number field sieve for integers of low weight", Oliver Schirokauer.
             */
            if (WNafUtil.getNafWeight(d) < minWeight)
            {
                continue;
            }

            break;
        }

        ECPoint Q = createBasePointMultiplier().multiply(params.getG(), d);

        return new AsymmetricCipherKeyPair(
            new ECPublicKeyParameters(Q, params),
            new ECPrivateKeyParameters(d, params));
    }

    protected ECMultiplier createBasePointMultiplier()
    {
        return new FixedPointCombMultiplier();
    }
}

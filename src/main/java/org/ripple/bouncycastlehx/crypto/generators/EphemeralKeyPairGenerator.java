package org.ripple.bouncycastlehx.crypto.generators;

import org.ripple.bouncycastlehx.crypto.AsymmetricCipherKeyPair;
import org.ripple.bouncycastlehx.crypto.AsymmetricCipherKeyPairGenerator;
import org.ripple.bouncycastlehx.crypto.EphemeralKeyPair;
import org.ripple.bouncycastlehx.crypto.KeyEncoder;

public class EphemeralKeyPairGenerator
{
    private AsymmetricCipherKeyPairGenerator gen;
    private KeyEncoder keyEncoder;

    public EphemeralKeyPairGenerator(AsymmetricCipherKeyPairGenerator gen, KeyEncoder keyEncoder)
    {
        this.gen = gen;
        this.keyEncoder = keyEncoder;
    }

    public EphemeralKeyPair generate()
    {
        AsymmetricCipherKeyPair eph = gen.generateKeyPair();

        // Encode the ephemeral public key
         return new EphemeralKeyPair(eph, keyEncoder);
    }
}

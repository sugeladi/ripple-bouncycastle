package org.ripple.bouncycastlehx.crypto;

import org.ripple.bouncycastlehx.crypto.params.AsymmetricKeyParameter;

public interface KeyEncoder
{
    byte[] getEncoded(AsymmetricKeyParameter keyParameter);
}

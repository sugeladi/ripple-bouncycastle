package org.ripple.bouncycastlehx.math.ec.endo;

import org.ripple.bouncycastlehx.math.ec.ECPointMap;

public interface ECEndomorphism
{
    ECPointMap getPointMap();

    boolean hasEfficientPointMap();
}

package org.ripple.bouncycastlehx.math.ec;

public interface ECPointMap
{
    ECPoint map(ECPoint p);
}

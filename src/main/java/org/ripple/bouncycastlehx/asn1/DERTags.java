package org.ripple.bouncycastlehx.asn1;

/**
 * @deprecated use BERTags
 */
public interface DERTags
    extends BERTags
{
}

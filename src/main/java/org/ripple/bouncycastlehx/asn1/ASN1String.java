package org.ripple.bouncycastlehx.asn1;

public interface ASN1String
{
    public String getString();
}

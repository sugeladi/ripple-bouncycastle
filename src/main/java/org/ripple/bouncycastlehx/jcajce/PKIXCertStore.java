package org.ripple.bouncycastlehx.jcajce;

import java.security.cert.Certificate;
import java.util.Collection;

import org.ripple.bouncycastlehx.util.Selector;
import org.ripple.bouncycastlehx.util.Store;
import org.ripple.bouncycastlehx.util.StoreException;

public interface PKIXCertStore<T extends Certificate>
    extends Store<T>
{
    Collection<T> getMatches(Selector<T> selector)
        throws StoreException;
}

package org.ripple.bouncycastlehx.jcajce.provider.symmetric.util;

import org.ripple.bouncycastlehx.crypto.BlockCipher;

public interface BlockCipherProvider
{
    BlockCipher get();
}

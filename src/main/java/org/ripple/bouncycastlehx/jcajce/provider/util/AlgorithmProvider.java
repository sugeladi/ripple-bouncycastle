package org.ripple.bouncycastlehx.jcajce.provider.util;

import org.ripple.bouncycastlehx.jcajce.provider.config.ConfigurableProvider;

public abstract class AlgorithmProvider
{
    public abstract void configure(ConfigurableProvider provider);
}

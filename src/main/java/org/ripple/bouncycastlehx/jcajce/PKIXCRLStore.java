package org.ripple.bouncycastlehx.jcajce;

import java.security.cert.CRL;
import java.util.Collection;

import org.ripple.bouncycastlehx.util.Selector;
import org.ripple.bouncycastlehx.util.Store;
import org.ripple.bouncycastlehx.util.StoreException;

public interface PKIXCRLStore<T extends CRL>
    extends Store<T>
{
    Collection<T> getMatches(Selector<T> selector)
        throws StoreException;
}

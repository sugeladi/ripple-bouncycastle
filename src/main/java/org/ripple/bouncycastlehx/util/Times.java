package org.ripple.bouncycastlehx.util;

public final class Times
{
    public static long nanoTime()
    {
        return System.nanoTime();
    }
}

package org.ripple.bouncycastlehx.util.test;

public interface Test
{
    String getName();

    TestResult perform();
}

package org.ripple.bouncycastlehx.jce.provider;

public class PKIXNameConstraintValidatorException
    extends Exception
{
    public PKIXNameConstraintValidatorException(String msg)
    {
        super(msg);
    }
}

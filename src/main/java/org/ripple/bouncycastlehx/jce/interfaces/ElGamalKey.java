package org.ripple.bouncycastlehx.jce.interfaces;

import org.ripple.bouncycastlehx.jce.spec.ElGamalParameterSpec;

public interface ElGamalKey
{
    public ElGamalParameterSpec getParameters();
}

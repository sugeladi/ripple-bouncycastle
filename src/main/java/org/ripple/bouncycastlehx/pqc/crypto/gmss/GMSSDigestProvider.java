package org.ripple.bouncycastlehx.pqc.crypto.gmss;

import org.ripple.bouncycastlehx.crypto.Digest;

public interface GMSSDigestProvider
{
    Digest get();
}

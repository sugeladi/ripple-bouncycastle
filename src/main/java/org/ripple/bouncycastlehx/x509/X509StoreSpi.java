package org.ripple.bouncycastlehx.x509;

import org.ripple.bouncycastlehx.util.Selector;

import java.util.Collection;

public abstract class X509StoreSpi
{
    public abstract void engineInit(X509StoreParameters parameters);

    public abstract Collection engineGetMatches(Selector selector);
}

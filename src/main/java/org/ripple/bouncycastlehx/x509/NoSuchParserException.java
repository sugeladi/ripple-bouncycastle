package org.ripple.bouncycastlehx.x509;

public class NoSuchParserException
    extends Exception
{
    public NoSuchParserException(String message)
    {
        super(message);
    }
}

package org.ripple.bouncycastlehx.x509;

public class NoSuchStoreException
    extends Exception
{
    public NoSuchStoreException(String message)
    {
        super(message);
    }
}
